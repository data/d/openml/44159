# OpenML dataset: covertype

https://www.openml.org/d/44159

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark,  
                          transformed in the same way. This dataset belongs to the "classification on categorical and
                          numerical features" benchmark. Original description: 
 
**Author**: Jock A. Blackard, Dr. Denis J. Dean, Dr. Charles W. Anderson  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Covertype) - 1998  

This is the original version of the famous covertype dataset in ARFF format. 

**Covertype**  
Predicting forest cover type from cartographic variables only (no remotely sensed data). The actual forest cover type for a given observation (30 x 30 meter cell) was determined from US Forest Service (USFS) Region 2 Resource Information System &#40;RIS&#41; data. Independent variables were derived from data originally obtained from US Geological Survey (USGS) and USFS data. Data is in raw form (not scaled) and contains binary (0 or 1) columns of data for qualitative independent variables (wilderness areas and soil types). 

This study area includes four wilderness areas located in the Roosevelt National Forest of northern Colorado. These areas represent forests with minimal human-caused disturbances, so that existing forest cover types are more a result of ecological processes rather than forest management practices. 

Some background information for these four wilderness areas: Neota (area 2) probably has the highest mean elevational value of the 4 wilderness areas. Rawah (area 1) and Comanche Peak (area 3) would have a lower mean elevational value, while Cache la Poudre (area 4) would have the lowest mean elevational value. 

As for primary major tree species in these areas, Neota would have spruce/fir (type 1), while Rawah and Comanche Peak would probably have lodgepole pine (type 2) as their primary species, followed by spruce/fir and aspen (type 5). Cache la Poudre would tend to have Ponderosa pine (type 3), Douglas-fir (type 6), and cottonwood/willow (type 4). 

The Rawah and Comanche Peak areas would tend to be more typical of the overall dataset than either the Neota or Cache la Poudre, due to their assortment of tree species and range of predictive variable values (elevation, etc.) Cache la Poudre would probably be more unique than the others, due to its relatively low elevation range and species composition.

Attribute Information:  
Given is the attribute name, attribute type, the measurement unit and a brief description. The forest cover type is the classification problem. The order of this listing corresponds to the order of numerals along the rows of the database. 
>
Name / Data Type / Measurement / Description  
Elevation / quantitative /meters / Elevation in meters  
Aspect / quantitative / azimuth / Aspect in degrees azimuth  
Slope / quantitative / degrees / Slope in degrees  
Horizontal_Distance_To_Hydrology / quantitative / meters / Horz Dist to nearest surface water features  
Vertical_Distance_To_Hydrology / quantitative / meters / Vert Dist to nearest surface water features  
Horizontal_Distance_To_Roadways / quantitative / meters / Horz Dist to nearest roadway  
Hillshade_9am / quantitative / 0 to 255 index / Hillshade index at 9am, summer solstice  
Hillshade_Noon / quantitative / 0 to 255 index / Hillshade index at noon, summer solstice  
Hillshade_3pm / quantitative / 0 to 255 index / Hillshade index at 3pm, summer solstice  
Horizontal_Distance_To_Fire_Points / quantitative / meters / Horz Dist to nearest wildfire ignition points  
Wilderness_Area (4 binary columns) / qualitative / 0 (absence) or 1 (presence) / Wilderness area designation  
Soil_Type (40 binary columns) / qualitative / 0 (absence) or 1 (presence) / Soil Type designation  
Cover_Type (7 types) / integer / 1 to 7 / Forest Cover Type designation 


Relevant Papers:  
- Blackard, Jock A. and Denis J. Dean. 2000. "Comparative Accuracies of Artificial Neural Networks and Discriminant Analysis in Predicting Forest Cover Types from Cartographic Variables." Computers and Electronics in Agriculture 24(3):131-151. 
- Blackard, Jock A. and Denis J. Dean. 1998. "Comparative Accuracies of Neural Networks and Discriminant Analysis in Predicting Forest Cover Types from Cartographic Variables." Second Southern Forestry GIS Conference. University of Georgia. Athens, GA. Pages 189-199. 
- Blackard, Jock A. 1998. "Comparison of Neural Networks and Discriminant Analysis in Predicting Forest Cover Types." Ph.D. dissertation. Department of Forest Sciences. Colorado State University. Fort Collins, Colorado. 165 pages.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44159) of an [OpenML dataset](https://www.openml.org/d/44159). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44159/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44159/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44159/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

